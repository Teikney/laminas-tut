<?php

namespace Blog\Controller;

use Blog\Form\PostForm;
use Blog\Model\Post;
use Blog\Model\PostCommandInterface;
use Blog\Model\PostRepository;
use Blog\Model\PostRepositoryInterface;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class WriteController extends AbstractActionController
{
    /**
     * @var PostCommandInterface
     */
    private $command;

    /**
     * @var PostForm
     */
    private $form;

    /**
     * @var PostRepositoryInterface
     */
    private $repository;

    /**
     * @param PostCommandInterface $command
     * @param PostForm $form
     * @param PostRepositoryInterface $repository
     */
    public function __construct( PostCommandInterface $command, PostForm $form, PostRepositoryInterface $repository)
    {
        $this->command = $command;
        $this->form = $form;
        $this->repository = $repository;
    }

    public function addAction() {

        $request = $this->getRequest();
        $viewModel = new ViewModel([
            'form' => $this->form,
        ]);
        if (! $request->isPost()) {
            return $viewModel;
        }

        $this->form->setData($request->getPost());
        if (! $this->form->isValid()) {
            return $viewModel;
        }

        /*
        $data = $this->form->getData()['post'];
        $post = new Post($data['title'],$data['text']);
        */
        //Above code commented after adding "$this->setObject(new Post('',''));" at Form\PostFieldset::class and updated with the line below
        $post = $this->form->getData();

        try {
            $post = $this->command->insertPost($post);
        } catch (\Exception $e) {
            // An exception occurred; we may want to log this later and/or
            // report it to the user. For now, we'll just re-throw.
            throw $e;
        }

        return $this->redirect()->toRoute(
            'blog/detail',
            ['id' => $post->getId()]
        );
    }

    public function editAction() {
        $id = $this->params()->fromRoute('id');
        $viewModel = new ViewModel([
            'form' => $this->form,
        ]);

        if (! $id) {
            return $this->redirect()->toRoute('blog');
        }

        try {
            $postToUpdate = $this->repository->findPost($id);
        } catch (\InvalidArgumentException $ex) {
            return $this->redirect()->toRoute('blog');
        }
        $this->form->bind($postToUpdate);

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return $viewModel;
        }

        $this->form->setData($request->getPost());
        if (! $this->form->isValid()) {
            return $viewModel;
        }

        $updatedPost = $this->command->updatePost($postToUpdate);
        return $this->redirect()->toRoute(
            'blog/detail',
            ['id' => $updatedPost->getId()],
        );
    }
}