<?php

namespace Blog\Model;

use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\Adapter\Driver\ResultInterface;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Insert;
use Laminas\Db\Sql\Sql;
use Laminas\Db\Sql\Update;
use RuntimeException;

class LaminasDbSqlCommand implements PostCommandInterface
{

    /**
     * @var AdapterInterface
     */
    private $db;

    /**
     * @param AdapterInterface $db
     */
    public function __construct(AdapterInterface $db)
    {
        $this->db = $db;
    }

    /**
     * {@inheritDoc}
     */
    public function insertPost(Post $post)
    {
        //Create a Laminas\Db\Sql\Insert instance, providing it the table name
        $insert = new Insert('posts');

        //Add values to the Insert instance
        $insert->values([
            'title' => $post->getTitle(),
            'text' => $post->getText(),
        ]);

        //Create a Laminas\Db\Sql\Sql instance with the database adapter
        $sql = new Sql($this->db);

        //Prepare a statement from our Insert instance
        $statement = $sql->prepareStatementForSqlObject($insert);

        //Execute the statement and check for a valid result
        $result = $statement->execute();
        if (! $result instanceof ResultInterface) {
            throw new RuntimeException(
                'Database error occurred during blog post insert operation'
            );
        }
        //Marshal a return value
        $id = $result->getGeneratedValue();
        return new Post( $post->getTitle(), $post->getText(), $id);
    }

    /**
     * {@inheritDoc}
     */
    public function updatePost(Post $post)
    {
        if (! $post->getId()) {
            throw new RuntimeException('Cannot update post; missing identifier');
        }

        $update = new Update('posts');
        $update->set([
            'title' => $post->getTitle(),
            'text' => $post->getText(),
        ]);
        $update->where(['id = ?' => $post->getId()]);

        $sql = new Sql($this->db);

        $statement = $sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();

        if(! $result instanceof ResultInterface) {
            throw new RuntimeException('Database error occurred during blog post update operation');
        }

        return $post;
    }

    /**
     * {@inheritDoc}
     */
    public function deletePost(Post $post)
    {
        if (! $post->getId()) {
            throw new RuntimeException('Cannot delete post; missing identifier');
        }

        $delete = new Delete('posts');
        $delete->where(['id = ?' => $post->getId()]);

        $sql = new Sql($this->db);
        $statement = $sql->prepareStatementForSqlObject($delete);
        $result = $statement->execute();

        if(! $result instanceof ResultInterface) {
            return false;
        }

        return true;
    }
}