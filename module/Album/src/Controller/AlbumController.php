<?php

namespace Album\Controller;

use Album\Form\AlbumForm;
use Album\Model\AlbumTable;
use Album\Model\Album;
use Exception;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class AlbumController extends AbstractActionController
{
    private $table;

    public function __construct(AlbumTable $table)
    {
        $this->table = $table;
    }

    public function indexAction() {
        return new ViewModel([
            'albums' => $this->table->fetchAll(),
        ]);
    }

    public function addAction() {

        /**
         * We instantiate AlbumForm and set the label on the
         * submit button to "Add".
         *
         * We do this here as we'll want to re-use the form
         * when editing an album and will use a different label.
         */
        $form = new AlbumForm();
        $form->get('submit')->setValue('Add');

        /**
         * If the request is not a POST request, then no form data
         * has been submitted, and we need to display the form.
         *
         * Laminas-mvc allows you to return an array of data
         * instead of a view model if desired;
         *
         * if you do, the array will be used to create a view model.
         */
        $request = $this->getRequest();
        if (! $request->isPost()) {
            return ['form'=> $form];
        }

        /**
         * At this point, we know we have a form submission.
         *
         * We create an Album instance, and pass its input filter
         * on to the form;
         *
         * Additionally, we pass the submitted data from the request
         * instance to the form.
         */
        $album = new Album();
        $form->setInputFilter($album->getInputFilter());
        $form->setData($request->getPost());


        /**
         * If form validation fails, we want to redisplay the form.
         *
         * At this point, the form contains information about what
         * fields failed validation, and why, and this information
         * will be communicated to the view layer.
         */
        if (!$form->isValid()) {
            return ['form' => $form];
        }

        /**
         * If the form is valid, then we grab the data from the form
         * and store to the model using saveAlbum().
         */
        $album->exchangeArray($form->getData());
        $this->table->saveAlbum($album);

        /**
         * After we have saved the new album row, we redirect back to
         * the list of albums using the Redirect controller plugin.
         */
        return $this->redirect()->toRoute('album');
    }

    public function editAction()
    {
        /**
         * params is a controller plugin that provides a convenient way
         * to retrieve parameters from the matched route.
         *
         * We use it to retrieve the id from the route we created within
         * the Album module's module.config.php.
         *
         * If the id is zero, then we redirect to the add action, otherwise,
         * we continue by getting the album entity from the database.
         *
         *
         */
        $id = (int) $this->params()->fromRoute('id', 0);
        if (0 === $id) {
            return $this->redirect()->toRoute('album', ['action' => 'add']);
        }

        /**
         * Retrieve the album with the specified id. Doing so raises
         * an exception if the album is not found, which should result
         * in redirecting to the landing page.
         *
         *
         * We have to check to make sure that the album with
         * the specified id can actually be found.
         *
         * If it cannot, then the data access method throws an exception.
         *
         * We catch that exception and re-route the user to the index page.
         */
        try {
            $album = $this->table->getAlbum($id);
        } catch (Exception $e) {
            return $this->redirect()->toRoute('album', ['action' => 'index']);
        }

        /**
         * The form's bind() method attaches the model to the form.
         *
         * This is used in two ways:
         *  - When displaying the form, the initial values for each element
         *  are extracted from the model.
         *  - After successful validation in isValid(), the data from the
         *  form is put back into the model.
         *
         * These operations are done using a hydrator object.
         * There are a number of hydrators, but the default one is
         * Laminas\Hydrator\ArraySerializable which expects to find two
         * methods in the model: getArrayCopy() and exchangeArray().
         */
        $form = new AlbumForm();
        $form->bind($album);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $form->setInputFilter($album->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return $viewData;
        }

        try {
            $this->table->saveAlbum($album);
        } catch (Exception $e) {
        }

        // Redirect to album list
        return $this->redirect()->toRoute('album', ['action' => 'index']);
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('album');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->table->deleteAlbum($id);
            }

            // Redirect to list of albums
            return $this->redirect()->toRoute('album');
        }

        return [
            'id'    => $id,
            'album' => $this->table->getAlbum($id),
        ];
    }
}