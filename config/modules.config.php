<?php

/**
 * List of enabled modules for this application.
 *
 * This should be an array of module namespaces used in the application.
 */
return [
    'Laminas\Mvc\I18n',
    'Laminas\Form',
    'Laminas\I18n',
    'Laminas\InputFilter',
    'Laminas\Filter',
    'Laminas\Hydrator',
    'Laminas\Db',
    'Laminas\Cache',
    'Laminas\Router',
    'Laminas\Validator',
    'Laminas\ZendFrameworkBridge',
    'Laminas\DeveloperTools',
    'Application',
    'Album',
    'Blog',
];
